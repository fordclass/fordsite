<?php
	Error_Reporting(E_ALL & ~E_NOTICE);
	session_start();

    include ("lib/functions.php");
    include ("lib/mysql.php");
    include ("application/core.php");
	$base = new MySql_;

	require("lib/Smarty-3.1.12/libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = $_SERVER['DOCUMENT_ROOT'].'/application/view';
	$smarty->compile_dir = $_SERVER['DOCUMENT_ROOT'].'/application/view/templates_c';

	core::router ();

	$smarty->assign('width', $width);
	$smarty->assign('height', $height);
	$smarty->assign('templates', $templates);
	$smarty->assign('Error', $Error);

	$smarty->display('Index.tpl');

?>