<div style="padding:0px 10px 0px 20px;width:100%;color:#ffffff;font-size: 13px;font-family: Verdana, Arial, Helvetica, sans-serif;">
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='index'}#AED9FF{else}#003d79{/if};"
		onMouseOver="this.style.backgrond='#AED9FF';"
		onMouseOut="this.style.background='{if $MenuItem=='index'}#AED9FF{else}#003d79{/if}';">
		<i class="icon-home {if $MenuItem!='index'}icon-white{/if}"></i>
		<a href="/" style="{if $MenuItem=='index'}color:#000000;{else}color:#ffffff;{/if}">Главная</a>
	</div>
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='company'}#AED9FF{else}#003d79{/if};">
		<i class="icon-flag {if $MenuItem!='company'}icon-white{/if}"></i>
		<a onclick="ShowElementInMenu('company');" style="{if $MenuItem=='company'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">О компании</a>
	</div>
		<div id="company" class="{if $MenuItem=='company'}_preload{else}preload{/if}">
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='contacts'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='contacts'}icon-white{/if}"></i> <a href="/company/contacts/" style="{if $SubMenuItem=='contacts'}color:#000000;{else}color:#ffffff;{/if}">Контакты</a></div>
		</div>
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='catalog'}#AED9FF{else}#003d79{/if};">
		<i class="icon-shopping-cart {if $MenuItem!='catalog'}icon-white{/if}"></i>
		<a onclick="ShowElementInMenu('catalog');" style="{if $MenuItem=='catalog'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">Запчасти</a>
	</div>
		<div id="catalog" class="{if $MenuItem=='catalog'}_preload{else}preload{/if}">
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='ford'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='ford'}icon-white{/if}"></i> <a href="/catalog/ford/" style="{if $SubMenuItem=='ford'}color:#000000;{else}color:#ffffff;{/if}">Ford наличие</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='mazda'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='mazda'}icon-white{/if}"></i> <a href="/catalog/ford/" style="{if $SubMenuItem=='mazda'}color:#000000;{else}color:#ffffff;{/if}">Mazda наличие</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='search'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='search'}icon-white{/if}"></i> <a href="/catalog/search/" style="{if $SubMenuItem=='search'}color:#000000;{else}color:#ffffff;{/if}">Поиск по номеру</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='vin'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='vin'}icon-white{/if}"></i> <a href="/catalog/vin/" style="{if $SubMenuItem=='vin'}color:#000000;{else}color:#ffffff;{/if}">Запрос по VIN</a></div>
		</div>
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='remont'}#AED9FF{else}#003d79{/if};">
		<i class="icon-wrench {if $MenuItem!='remont'}icon-white{/if}"></i>
		<a onclick="ShowElementInMenu('remont');" style="{if $MenuItem=='remont'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">Ремонт</a>
	</div>
		<div id="remont" class="{if $MenuItem=='remont'}_preload{else}preload{/if}">
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='ford'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='infoto'}icon-white{/if}"></i> <a href="/remont/infoto/" style="{if $SubMenuItem=='infoto'}color:#000000;{else}color:#ffffff;{/if}">Рекомендации ТО</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='to'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='to'}icon-white{/if}"></i> <a href="/remont/to/" style="{if $SubMenuItem=='to'}color:#000000;{else}color:#ffffff;{/if}">ТО-техн. обслуж.</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='slesar'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='slesar'}icon-white{/if}"></i> <a href="/remont/slesar/" style="{if $SubMenuItem=='slesar'}color:#000000;{else}color:#ffffff;{/if}">Слесарный ремонт</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='engine'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='engine'}icon-white{/if}"></i> <a href="/remont/engine/" style="{if $SubMenuItem=='engine'}color:#000000;{else}color:#ffffff;{/if}">Ремонт двигателя</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='kuzov'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='kuzov'}icon-white{/if}"></i> <a href="/remont/kuzov/" style="{if $SubMenuItem=='kuzov'}color:#000000;{else}color:#ffffff;{/if}">Кузовной ремонт</a></div>
		</div>
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='sales'}#AED9FF{else}#003d79{/if};">
		<i class="icon-gift {if $MenuItem!='sales'}icon-white{/if}"></i>
		<a onclick="ShowElementInMenu('sales');" style="{if $MenuItem=='sales'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">Скидки</a>
	</div>
		<div id="sales" class="{if $MenuItem=='sales'}_preload{else}preload{/if}">
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='specials'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='specials'}icon-white{/if}"></i> <a href="/sales/specials/" style="{if $SubMenuItem=='specials'}color:#000000;{else}color:#ffffff;{/if}">Спецпредложения</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='supersale'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='supersale'}icon-white{/if}"></i> <a href="/sales/supersale/" style="{if $SubMenuItem=='supersale'}color:#000000;{else}color:#ffffff;{/if}">Супер скидка</a></div>
		</div>
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='dostavka'}#AED9FF{else}#003d79{/if};">
		<i class="icon-plane {if $MenuItem!='dostavka'}icon-white{/if}"></i>
		<a href="/dostavka/" onclick="ShowElementInMenu('dostavka');" style="{if $MenuItem=='dostavka'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">Доставка</a>
	</div>
	<div
		class="menuitem1"
		style="background-color:{if $MenuItem=='profile'}#AED9FF{else}#003d79{/if};">
		<i class="icon-edit {if $MenuItem!='profile'}icon-white{/if}"></i>
		<a onclick="ShowElementInMenu('profile');" style="{if $MenuItem=='profile'}color:#000000;{else}color:#ffffff;{/if}cursor:pointer;">Личный кабинет</a>
	</div>
		<div id="profile" class="{if $MenuItem=='profile'}_preload{else}preload{/if}">
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='all'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='all'}icon-white{/if}"></i> <a href="/profile/all/" style="{if $SubMenuItem=='all'}color:#000000;{else}color:#ffffff;{/if}">Общие</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='auto'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='auto'}icon-white{/if}"></i> <a href="/profile/auto/" style="{if $SubMenuItem=='auto'}color:#000000;{else}color:#ffffff;{/if}">Мои автомобили</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='orders'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='orders'}icon-white{/if}"></i> <a href="/profile/orders/" style="{if $SubMenuItem=='orders'}color:#000000;{else}color:#ffffff;{/if}">Заказы</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='remont'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='remont'}icon-white{/if}"></i> <a href="/profile/remont/" style="{if $SubMenuItem=='remont'}color:#000000;{else}color:#ffffff;{/if}">История ремонта</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='vin'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='vin'}icon-white{/if}"></i> <a href="/profile/vin/" style="{if $SubMenuItem=='vin'}color:#000000;{else}color:#ffffff;{/if}">Запросы по VIN</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='trash'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='trash'}icon-white{/if}"></i> <a href="/profile/trash/" style="{if $SubMenuItem=='trash'}color:#000000;{else}color:#ffffff;{/if}">Корзина</a></div>
			<div class="menuitem2" style="background-color:{if $SubMenuItem=='money'}#AED9FF{else}#003d79{/if};"><i class="icon-play {if $SubMenuItem!='money'}icon-white{/if}"></i> <a href="/profile/money/" style="{if $SubMenuItem=='money'}color:#000000;{else}color:#ffffff;{/if}">Баланс</a></div>
		</div>
</div>