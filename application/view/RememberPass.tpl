<form class="well" method="post" action="/users/do_rememberPass/" style="font-size:12px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#000000;">
	<div>Уважаемый пользователь, Вы уже зарегестрированы на нашем портале.</div>
	<div>Если вы забыли пароль, воспользуйтесь формой восстановления пароля. Необходимо ввести код с картинки и нажать на кнопку.<br>
	На Ваш электронный адрес, указанный при регистрации, будет выслан пароль.
	</div>
	<br><br>
	{if $MsgBool==1}<div style="font-size:14px;color:#FF3C3C;">Письмо с паролем отправлено на Вашу электронную почту.</div>{/if}
	<div>
        <div><input type="text" class="span3" name="RegMail" value="{$smarty.post.RegMail}" style="height:18px;" onBlur="if(!CheckEmail(this.form.RegMail)){ this.form.RegMail.focus(); }"> E-mail (логин) {if $Error.Mail!=''}<b style="color:#FF3C3C">{$Error.Mail}</b>{/if}</div>
		<div><input type="text" class="span3" name="keystring" value="" style="height:18px;" required> Введите текст с картинки {if $Error.Kod!=''}<b style="color:#FF3C3C">{$Error.Kod}</b>{/if}</div>
		<div><img src="/lib/kcaptcha/index.php" border="0"></div>
		<input type="submit" class="btn btn-success" value="Восстановить пароль">

	</div>
</form>