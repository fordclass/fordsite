<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="/lib/mycss.css">
	<link rel="stylesheet" href="/lib/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="/lib/bootstrap/css/bootstrap-responsive.css">

	<title></title>

	<meta name="keywords" content="">
	<meta name= "description" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1.0"> <!--��� ����������� ����������� � ������ ������������ �����-��������, ���������� �������� viewport meta - ���.-->

	<meta http-equiv="expires" content="Tue, 30 January 2011 12:00:00 GMT">
	<meta  name="address" content="г.Москва, Волоколамское ш. д.89 корп.1 стр.1">

	{literal}
    <script type="text/javascript" src="/lib/functions.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<script type='text/javascript'>
		function setFullCookie (name, value, expires, path, domain, secure) {
    		document.cookie = name + "=" + escape(value) +
    		((expires) ? "; expires=" + expires : "") +
    		((path) ? "; path=" + path : "") +
    		((domain) ? "; domain=" + domain : "") +
    		((secure) ? "; secure" : "");
		}

		function readCookie(name) {
    		var nameEQ = name + "=";
    		var ca = document.cookie.split(';');
    		for(var i=0;i < ca.length;i++) {
    		    var c = ca;
    		    while (c.charAt(0)==' ') c = c.substring(1,c.length);
     		   if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
   			}
   			return null;
		}

		function removeCookie(name, path) {
		    var date = new Date ( );
		    date.setTime(date.getTime() - 1);
		    document.cookie = name += "=; expires=" + date.toGMTString() + ((path) ? "; path=" + path : "/");
		}

		setFullCookie( "width", $(window).width() , "Mon, 01-Jan-2018 00:00:00 GMT", "/");
		setFullCookie( "height", $(window).height() , "Mon, 01-Jan-2018 00:00:00 GMT", "/");
		var w = readCookie("width");
		if(w=='') { window.location.href = "/"; }
	</script>
	{/literal}


</head>
<body bgcolor="#003d79">





<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
	<tr height="40px">
		<td width="200px" rowspan="2" align="left" valign="top">
			{if $smarty.session.Sotrudnik or $smarty.session.Client} {include file="disAutorisation.tpl"} {else} {include file="Autorisation.tpl"} {/if}
			{if !$smarty.session.Sotrudnik} {include file="MenuForClient.tpl"} {/if}
			{foreach from=$templates[1] item=tpl}{include file="`$tpl`.tpl"}{/foreach}
		</td>
		<td width="940px"><font color="#ffffff" style="font-family: Verdana, Arial, Helvetica, sans-serif;">Котировки на сегодня {$Today}: USD <b class="btn btn-info">{number_format($USD, 4, ',', ' ')}</b> EUR <b class="btn btn-info">{number_format($EUR, 4, ',', ' ')}</b></font></td>
		<td width="{$width-250-940}px" rowspan="2"> </td>
	</tr>
	<tr>
		<td style="height:{$height-100}px;-webkit-border-radius:3px;border-radius:3px;" bgcolor="#e5e5e5" align="left" valign="top">
			{foreach from=$templates[2] item=tpl}{include file="`$tpl`.tpl"}{/foreach}
		</td>
	</tr>
</table>

</body>
</html>