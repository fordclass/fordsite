<form class="well" method="post" action="/users/do_register/" style="font-size:12px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#000000;">
	<div><b style="font-size:16px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#000000;">Регистрация нового пользователя:</b></div>
	<br>
	<div>
	Порядок регистрации:<br>
	-заполнить данную форму (ВСЕ ПОЛЯ ОБЯЗАТЕЛЬНЫ ДЛЯ ЗАПОЛНЕНИЯ)<br>
	-подтвердить регистрацию, нажав на ссылку в письме, которое высылается на ваш E-mail сразу же после нажатия на кнопку Далее<br>
	-получить пароль в письме, которое будет вам отправлено сразу же как только Вы подтвердите регистрацию.<br><br>
	Остальные данные о себе, об автомобиле, об организации, если вы фирма, Вы можете отредактировать позже в личном кабинете.
	</div>
	<label class="checkbox">
	<input id="id_urlico" type="checkbox" name="RegUrLico" value="1" onClick="ShowElementInReg();" {if $RegUrLico EQ 1}checked{/if}> <font style="font-size:12px;font-family:Verdana, Arial, Helvetica, sans-serif;color:#0000ff;">если Вы собирайтесь работать с нами как юридическое лицо, поставьте галочку</font>
	</label>

	<div>{if $Error.Reg!=''}<b style="color:#FF3C3C">{$Error.Reg}</b>{/if}</div>
	<div><input type="text" class="span3" name="RegMail" value="{$smarty.post.RegMail}" style="height:18px;" onBlur="if(!CheckEmail(this.form.RegMail)){ this.form.RegMail.focus(); }"> E-mail (логин) {if $Error.Mail!=''}<b style="color:#FF3C3C">{$Error.Mail}</b>{/if}</div>
	<div><input type="text" class="span3" name="RegPass" value="{$smarty.post.RegPass}" style="height:18px;"> Пароль {if $Error.Pass!=''}<b style="color:#FF3C3C">{$Error.Pass}</b>{/if}</div>
	<div><input type="text" class="span3" name="RegPass2" value="{$smarty.post.RegPass2}" style="height:18px;"> Подтвердите пароль {if $Error.Pass!=''}<b style="color:#FF3C3C">{$Error.Pass}</b>{/if}</div>
	<div id="id_RegFamiliya"><input type="text" class="span3" name="RegFamiliya" value="{$smarty.post.RegFamiliya}" style="height:18px;" onBlur="CheckText(this.form.RegFamiliya);"> Фамилия {if $Error.Familiya!=''}<b style="color:#FF3C3C">{$Error.Familiya}</b>{/if}</div>
	<div id="id_RegImya"><input type="text" class="span3" name="RegImya" value="{$smarty.post.RegImya}"      style="height:18px;" onBlur="CheckText(this.form.RegImya);"> Имя {if $Error.Imya!=''}<b style="color:#FF3C3C">{$Error.Imya}</b>{/if}</div>
	<div id="id_RegOtchestvo"><input type="text" class="span3" name="RegOtchestvo" value="{$smarty.post.RegOtchestvo}" style="height:18px;" onBlur="CheckText(this.form.RegOtchestvo);"> Отчество {if $Error.Otchestvo!=''}<b style="color:#FF3C3C">{$Error.Otchestvo}</b>{/if}</div>
	<div id="id_RegName" class="preload"><input type="text" class="span3" name="RegName"      value="{$smarty.post.RegName}"      style="height:18px;"> Название организации {if $Error.Name!=''}<b style="color:#FF3C3C">{$Error.Name}</b>{/if}</div>
	<div><input type="text" class="span3" name="RegTel1"      value="{$smarty.post.RegTel1}"      style="height:18px;" onBlur="if(!Formdata(this.form.RegTel1)){ this.form.RegTel1.focus(); }"> Телефон1 {if $Error.Tel1!=''}<b style="color:#FF3C3C">{$Error.Tel1}</b>{/if}</div>
	<div><input type="text" class="span3" name="RegTel2"      value="{$smarty.post.RegTel2}"      style="height:18px;" onBlur="if(!Formdata(this.form.RegTel2)){ this.form.RegTel2.focus(); }"> Телефон2</div>
	<div><input type="text" class="span3" name="keystring" value=""             style="height:18px;" required> Введите текст с картинки {if $Error.Kod!=''}<b style="color:#FF3C3C">{$Error.Kod}</b>{/if}</div>
	<div><img src="/lib/kcaptcha/index.php" border="0"></div>

	<input type="submit" class="btn btn-success" value="Далее">
</form>