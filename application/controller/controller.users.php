<?php

	//echo "<h1>".__FILE__."</h1>";

	class cUsers {
		public static function register() {        	core::addTemplate('Registration',2);		}

		public static function do_register () {
			core::useModel ('users');
			$mUser = new mUsers;
			$mUser->addUser();
		}

		public static function do_rememberPass() {
			core::useModel ('users');
			$mUser = new mUsers;
			$mUser->RememberPass();
		}

		public static function do_login() {
			core::useModel ('users');
			$mUser = new mUsers;
			$mUser->doLogin();
		}

		public static function do_logout() {
			core::useModel ('users');
			$mUser = new mUsers;
			$mUser->doLogout();
		}

		public static function view() {
			//pr (__FUNCTION__);
			useModel ('users');
			$mUser = new mUsers;

			$users = $mUser->getAllUsers();
			//pr ($users);
		}

		public static function index() {
			self::register();
		}

	}

?>